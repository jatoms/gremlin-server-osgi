package io.jatoms.gremlin.server.example.api;

public interface GreeterService {
    public String greet(String toGreet);
}