package io.jatoms.gremlin.server.example.basic;

import io.jatoms.gremlin.server.example.api.GreeterService;
import org.osgi.service.component.annotations.Component;

@Component
public class BasicGreeter implements GreeterService {
    
    @Override
    public String greet(String toGreet) {
        return "Hello " + toGreet;
    }
}